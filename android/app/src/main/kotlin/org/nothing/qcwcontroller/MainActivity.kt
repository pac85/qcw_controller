package org.nothing.qcwcontroller

import android.os.Bundle

import java.io.*
import java.util.*

import android.bluetooth.*
import android.content.Intent
import android.util.Log

import java.util.ArrayList
import java.util.regex.Pattern;


import io.flutter.app.FlutterActivity
import io.flutter.plugins.GeneratedPluginRegistrant
import io.flutter.plugin.common.MethodChannel

class MainActivity() : FlutterActivity() {
    private val CHANNEL = "qcw_native/bluetooth"

    private val REQUEST_ENABLE_BT = 1
    private val MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")

    private lateinit var bluetooth_adapter: BluetoothAdapter
    private lateinit var paired_devices: Set<BluetoothDevice>
    private var paired_devices_list = ArrayList<BluetoothDevice>();
    private lateinit var bluetooth_socket: BluetoothSocket
    private lateinit var bt_output_stream: OutputStream

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        GeneratedPluginRegistrant.registerWith(this)

        bluetooth_adapter = BluetoothAdapter.getDefaultAdapter()
        if (bluetooth_adapter == null) return
        else {
            if (!bluetooth_adapter.isEnabled()) {
                val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
            }

            paired_devices = bluetooth_adapter.getBondedDevices() as Set<BluetoothDevice>
            for (device in paired_devices)
                paired_devices_list.add(device)


        }



        MethodChannel(flutterView, CHANNEL).setMethodCallHandler { call, result ->
            if (call.method == "getDevices") {
                var device_names = ArrayList<String>()
                for (device in paired_devices)
                    device_names.add(device.getName())
                if (!device_names.isEmpty())
                    result.success(device_names)
                else {
                    result.error("UNAVAILABLE", "no devices", null)
                }
            }
            if (call.method == "tryConnection") {
                try {
                    var device_index: Int = call.arguments()
                    Log.i("bt", "trying connecing to device number " + device_index)
                    var device = paired_devices_list[device_index]
                    bluetooth_socket = device.createRfcommSocketToServiceRecord(MY_UUID)//createInsecureRfcommSocketToServiceRecord
                    if(bluetooth_socket == null)
                        result.error("UNAVAILABLE", "connection error: unable to open socket", null)
                    bluetooth_socket.connect();
                    bt_output_stream = bluetooth_socket.getOutputStream();
                    if(bt_output_stream == null)
                        result.error("UNAVAILABLE", "connection error: unable to open output stream", null)
                    Log.i("bt", "connected to device")
                    result.success(true)
                } catch (e: IOException) {
                    Log.i("bt", "connection error")
                    result.error("UNAVAILABLE", "connection error", null)
                }
            }
            if (call.method == "sendFire") {
                try {
                    var fire_command: String = getFireCommandJNI()
                    bt_output_stream.write(fire_command.toByteArray(Charsets.UTF_8))
                    result.success(true);
                }
                catch (e:IOException)
                {
                    result.error("BT ERROR", "transmission error", null);
                }

            }
            if (call.method == "sendSet") {
                try {
                    var propery_value: String = call.arguments();
                    var split_propery_value = propery_value.split(" ")
                    var set_command: String = getSetCommandJNI(split_propery_value[0], split_propery_value[1])
                    bt_output_stream.write(set_command.toByteArray(Charsets.UTF_8))
                    result.success(true);
                }
                catch (e:IOException)
                {
                    result.error("BT ERROR", "transmission error", null);
                }

            }
        }
    }

    external fun getFireCommandJNI(): String
    external fun getSetCommandJNI(property: String , value: String): String

    companion object {
        init {
            System.loadLibrary("command-gen-native");
        }
    }
}
