//
// Created by antonino on 31/07/18.
//
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <string>

using namespace std;

enum CmdType
{
    FIRE,
    SET,
    PLAY,
    NONE
};


struct Command
{
private:
    bool valid;
    CmdType cmd_type = NONE;
    char * args_str;

    Command(){}
public:
    Command(const char * cmd_str)
    {
        valid = false;

        //parses command
        char cmd_type_str[6];
        char checksum_str[5];
        char args_str[50];
        int res = sscanf(cmd_str,  "# %s # / %s %[^/] /", checksum_str, cmd_type_str, args_str);
        if(res == 0 || res == EOF) return;//the command is invalid therefore we wxit leaving valid to false

        //parses command type
        if(strcmp(cmd_type_str, "fire") == 0)
            cmd_type = FIRE;
        else if(strcmp(cmd_type_str, "play") == 0)
            cmd_type = PLAY;
        else if(strcmp(cmd_type_str, "set") == 0)
            cmd_type = SET;

        this->args_str = (char*)malloc(strlen(args_str)+1);
        strcpy(this->args_str, args_str);

        valid = true;;

    }

    static string get_fire_command()
    {
        Command tmp;
        tmp.cmd_type = FIRE;
        tmp.args_str = new char[1];
        tmp.args_str[0] = 0;
        tmp.valid = true;
        return string("@") + string(tmp.get_cmd_str()) + string("%");
    }

    static string get_set_command(string property, string val)
    {
        Command tmp;
        tmp.cmd_type = SET;
        tmp.args_str = (char*)malloc(property.length() + val.length()+1);
        strcpy(tmp.args_str, (property+" "+val).c_str());
        tmp.valid = true;
        return string("@") + string(tmp.get_cmd_str()) + string("%");
    }

    void process()
    {
        /*if(!valid) return;
        switch(cmd_type)
        {
            case FIRE:
                fire_callback();

                break;
            case SET:
            {
                //parses arguments string
                char property[10], value_str[10];
                sscanf(args_str, "%s %s", property, value_str);

                set_callback(property, value_str);

                break;
            }
            case PLAY:
                break;
            default:
                break;
        }*/
    }

    const char * get_cmd_str()
    {
        char command[95], cmd_type_str[6];
        switch(cmd_type)
        {
            case FIRE:
                strcpy(cmd_type_str ,"fire");
                break;
            case SET:
            {
                strcpy(cmd_type_str ,"set");
                break;
            }
            case PLAY:
                strcpy(cmd_type_str, "play");
                break;
            default:
                break;
        }
        snprintf(command, 95, " / %s %s / ", cmd_type_str, args_str);

        char checksum_str[5];
        strcpy(checksum_str, "123");

        char * final_command = (char*)malloc(strlen(command)+strlen(checksum_str)+2);
        snprintf(final_command, 100, "# %s # %s", checksum_str, command);
        return final_command;
    }

    ~Command()
    {
        delete(args_str);
    }

};


#include <jni.h>

extern "C"
{
    JNIEXPORT jstring JNICALL Java_org_nothing_qcwcontroller_MainActivity_getFireCommandJNI( JNIEnv* env, jobject thiz )
    {
        return env->NewStringUTF(Command::get_fire_command().c_str());
    }

    JNIEXPORT jstring JNICALL Java_org_nothing_qcwcontroller_MainActivity_getSetCommandJNI( JNIEnv* env, jobject thiz, jstring property, jstring value )
    {
        const char *native_property = env->GetStringUTFChars(property, 0);
        const char *native_value = env->GetStringUTFChars(value, 0);

        return env->NewStringUTF(Command::get_set_command(string(native_property), string(native_value)).c_str());
    }
}