import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class BluetoothSerial
{
    static const platform = const MethodChannel('qcw_native/bluetooth');
    Future<List<String>> getPairedDevices() async
    {
      try
      {
        final List<dynamic> result = await platform.invokeMethod('getDevices');
        return result.map((string){return string as String;}).toList();
      }
      on PlatformException catch (e) {
        print(e.message);
        return [""];
      }
      catch (e) {
        print(e.message);
        return [""];
      }
    }

    Future<bool> tryConnection(int device_index) async
    {
      try {
        await platform.invokeMethod('tryConnection', device_index);
        return true;
      }
      on PlatformException catch (e) {
        return false;
      }
      catch (e) {
        print(e.message);
        return false;
      }
    }

    Future<bool> sendFire() async
    {
      try {
        await platform.invokeMethod('sendFire');
        return true;
      }
      on PlatformException catch (e) {
        return false;
      }
      catch (e) {
        print(e.message);
        return false;
      }
    }

    Future<bool> sendSet(String propery, String value) async
    {
      try
      {
        await platform.invokeMethod('sendSet', propery + " " + value);
        return true;
      }
      catch(e)
      {
        print(e.message);
        return false;
      }
    }
}